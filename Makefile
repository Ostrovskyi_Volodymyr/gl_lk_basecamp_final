DIR=$(PWD)
all: uniac-dsp uniac-mpu6050 final

uniac-dsp:
	cd $(DIR)/src/uniac-dsp && make
	cd $(DIR)

uniac-mpu6050:
	cd $(DIR)/src/uniac-mpu6050 && make
	cd $(DIR)

final:
	mkdir -p $(DIR)/bin
	gcc $(DIR)/src/main.c -lm -o $(DIR)/bin/final

clean:
	cd $(DIR)/src/uniac-dsp && make clean
	cd $(DIR)/src/uniac-mpu6050 && make clean
	rm -f $(DIR)/bin/final