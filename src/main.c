#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>  
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>

static int dsp_clear;
static int dsp_cmd;
static int mpu6050_accel_x;
static int mpu6050_accel_y;
static int mpu6050_accel_z;
static int rect_x, rect_y;

void draw_field(void) {
	write(dsp_cmd, "line;63;0;63;159", strlen("line;63;0;63;159"));
	write(dsp_cmd, "line;64;0;64;159", strlen("line;63;0;63;159"));
	write(dsp_cmd, "line;0;79;127;79", strlen("line;63;0;63;159"));
	write(dsp_cmd, "line;0;80;127;80", strlen("line;63;0;63;159"));
}

void draw_data(float x, float y) {
	char buf[256];
	sprintf(buf, "text;1;1;X:%2.1f", x);
	write(dsp_cmd, buf, strlen(buf));
	sprintf(buf, "text;1;12;Y:%2.1f", y);
	write(dsp_cmd, buf, strlen(buf));
}

void draw_rect(float x, float y) {
	char buf[128];
	sprintf(buf, "rect;%d;%d;10;10", rect_x, rect_y);
	write(dsp_cmd, "crgb;255;255;255", strlen("crgb;255;255;255"));
	write(dsp_cmd, buf, strlen(buf));
	write(dsp_cmd, "crgb;0;0;0", strlen("crgb;0;0;0"));
	draw_field();
	sprintf(buf, "updt;%d;%d;10;10", rect_x, rect_y);
	write(dsp_cmd, buf, strlen(buf));
	write(dsp_cmd, "crgb;50;100;0", strlen("crgb;50;100;0"));
	rect_x = fabs(x * 0.71);
	rect_y = fabs(y * 0.88);
	if (x < 0) {
		rect_x = 64 - rect_x;
	} else {
		rect_x += 64;
	}
	if (y < 0) {
		rect_y += 80;
	} else {
		rect_y = 80 - rect_y;
	}
	sprintf(buf, "rect;%d;%d;10;10", rect_x, rect_y);
	write(dsp_cmd, buf, strlen(buf));
	sprintf(buf, "updt;%d;%d;10;10", rect_x, rect_y);
	write(dsp_cmd, buf, strlen(buf));
	write(dsp_cmd, "crgb;0;0;0", strlen("crgb;0;0;0"));
}

void get_angles(float *x, float *y) {
	char buf[7];
	float rad_to_deg = 180/3.141592654;
	float rx, ry, rz;
	pread(mpu6050_accel_x, buf, 7, 0);
	rx = atof(buf);
	pread(mpu6050_accel_y, buf, 7, 0);
	ry = atof(buf);
	pread(mpu6050_accel_z, buf, 7, 0);
	rz = atof(buf);
	*x = (atan(ry/sqrt(pow(rx,2) + pow(rz,2)))*rad_to_deg);
	*y = (atan(-1*(rx)/sqrt(pow(ry,2) + pow(rz,2)))*rad_to_deg);
}

int main(void) {
	dsp_clear = open("/sys/class/uniac-dsp/clear", O_WRONLY);
	dsp_cmd = open("/sys/class/uniac-dsp/cmd", O_WRONLY);
	mpu6050_accel_x = open("/sys/class/mpu6050/accel_x", O_RDONLY);
	mpu6050_accel_y = open("/sys/class/mpu6050/accel_y", O_RDONLY);
	mpu6050_accel_z = open("/sys/class/mpu6050/accel_z", O_RDONLY);
	rect_x = 59;
	rect_y = 75;

	write(dsp_clear, "1", 1);
	draw_rect(0, 0);
	draw_field(); 
	draw_data(0, 0);
	write(dsp_cmd, "updt;0;0;128;160", strlen("updt;0;0;128;160"));

	float x, y;
	while (1) {
		get_angles(&x, &y);
		write(dsp_cmd, "crgb;255;255;255", strlen("crgb;255;255;255"));
		write(dsp_cmd, "rect;0;0;64;50", strlen("rect;0;0;64;50"));
		write(dsp_cmd, "crgb;0;0;0", strlen("crgb;0;0;0"));
		draw_field();
		draw_data(x, y);
		write(dsp_cmd, "updt;0;0;64;50", strlen("updt;0;0;64;50"));
		draw_rect(x, y);
		sleep(1);
	}

	close(mpu6050_accel_x);
	close(mpu6050_accel_y);
	close(mpu6050_accel_z);
	close(dsp_clear);
	close(dsp_cmd);
	return 0;
}