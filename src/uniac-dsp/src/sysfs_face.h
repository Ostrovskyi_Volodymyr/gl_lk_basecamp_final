#ifndef SYSFS_FACE_H_
#define SYSFS_FACE_H_

#include <linux/fs.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/version.h>
#include <linux/parport.h>
#include <linux/pci.h>
#include <linux/string.h>
#include <linux/slab.h>

#include "st7735/st7735.h"
#include "cmd_parser.h"

int sysfs_face_init(st7735_t *st);
void sysfs_face_exit(void);

#endif // SYSFS_FACE_H_