#ifndef ST7735_H_
#define ST7735_H_
#include <linux/kernel.h>
#include <linux/spi/spi.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <asm/uaccess.h>

#include "font.h"

typedef struct {
	uint8_t r, g, b;
} rgb_color_t;

typedef struct {
	struct gpio st_dc;
	struct spi_device *spi_dev;
	uint8_t *framebuf;
	uint32_t framebuf_size;
	uint8_t width, height;
} st7735_t;

int st_init(st7735_t *st, struct spi_device *spi, uint8_t DCpin);
void st_delete(st7735_t *st);
void st_write_byte(st7735_t *st, uint8_t b, uint8_t dc);
void st_write_word(st7735_t *st, uint16_t w);
void st_cmd(st7735_t *st, uint8_t cmd);
void st_write888(st7735_t *st, rgb_color_t value, int reps);
void st_window(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);
void st_draw_pixel(st7735_t *st, uint16_t x, uint16_t y, rgb_color_t color);
void st_fill_rect(st7735_t *st, uint16_t x, uint16_t y, uint16_t w, uint16_t h, rgb_color_t color);
void st_present(st7735_t *st, uint16_t x, uint16_t y, uint16_t w, uint16_t h);
void st_clear(st7735_t *st, rgb_color_t color);
void st_inversion(st7735_t *st, uint8_t on);
void st_put_char(st7735_t *st, char c, uint16_t x, uint16_t y, rgb_color_t color);
void st_put_string(st7735_t *st, const char *str, uint16_t x, uint16_t y, rgb_color_t color);
void st_put_int(st7735_t *st, int num, uint16_t x, uint16_t y, rgb_color_t color);
void st_draw_line(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, rgb_color_t color);

#define SWRESET 0x01
#define SLPOUT 0x11
#define DISPON 0x29
#define CASET 0x2A
#define RASET 0x2B
#define RAMWR 0x2C
#define INVON 0x21
#define INVOFF 0x20
#define MADCTL 0x36
#define COLMOD 0x3A
#define PWCTR3 0xC2

#define COLOR_12BIT 3
#define COLOR_16BIT 5
#define COLOR_18BIT 6

#define MAD_MY 0b10000000
#define MAD_MX 0b1000000
#define MAD_MV 0b100000
#define MAD_ML 0b10000
#define MAD_BGR 0b1000
#define MAD_MH 0b100
#define MAD_RGB 0

#define PWR3_AP_SML 0b1
#define PWR3_AP_MDL 0b10
#define PWR3_AP_MD 0b11
#define PWR3_AP_MDH 0b100
#define PWR3_AP_LRG 0b101
#define PWR3_DC_1_1 0
#define PWR3_DC_1_2 0b1
#define PWR3_DC_1_4 0b10
#define PWR3_DC_2_2 0b11
#define PWR3_DC_2_4 0b100
#define PWR3_DC_4_4 0b101
#define PWR3_DC_4_8 0b110
#define PWR3_DC_4_16 0b111

#endif // ST7735_H_