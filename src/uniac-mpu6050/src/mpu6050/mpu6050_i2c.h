#ifndef MPU6050_H_
#define MPU6050_H_

#include <linux/kernel.h>
#include <linux/i2c.h>
#include <linux/types.h>

#include "mpu6050_regs.h"

int mpu6050_check_device(const struct i2c_client *client);
void mpu6050_default_config(const struct i2c_client *client, int32_t prec);
int32_t mpu6050_precision(void);
int32_t mpu6050_temp(void);
int32_t mpu6050_accel_x(void);
int32_t mpu6050_accel_y(void);
int32_t mpu6050_accel_z(void);
int32_t mpu6050_gyro_x(void);
int32_t mpu6050_gyro_y(void);
int32_t mpu6050_gyro_z(void);

#endif // MPU6050_H_
