#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/device.h>
#include <linux/of.h>
#include <linux/moduleparam.h>

#include "mpu6050/mpu6050_i2c.h"
#include "sysfs_face.h"

static int precision = 1000;
module_param(precision, int, 0);

static const struct of_device_id id_table[] = {
	{.compatible = "uniac,mpu6050", },
	{},
};

MODULE_DEVICE_TABLE(of, id_table);

static int mpu6050_probe(struct i2c_client *client, const struct i2c_device_id *id) {
	const char *devname;
	if (!of_property_read_string(client->dev.of_node, "device-name", &devname)) {
		printk(KERN_INFO "%s(): Device name: %s\n", __func__, devname);
	}

	if (mpu6050_check_device(client)) {
		printk(KERN_INFO "%s(): MPU6050 connected!\n", __func__);
		mpu6050_default_config(client, precision);
		sysfs_face_init();
	}
	return 0;
}

static int mpu6050_remove(struct i2c_client *client) {
	printk(KERN_INFO "%s() call\n", __func__);
	sysfs_face_exit();
	return 0;
}

static struct i2c_driver mpu6050_driver = {
	.probe = mpu6050_probe,
	.remove = mpu6050_remove,
	.driver = {
		.name = "mpu6050",
		.owner = THIS_MODULE,
		.of_match_table = of_match_ptr(id_table), 
	},
};

module_i2c_driver(mpu6050_driver);
MODULE_LICENSE("GPL");